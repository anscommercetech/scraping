<?php

class Model {

    protected $db;
    protected $prefix;

    public function __construct() {
        $this->prefix = isset(Config::$db['prefix'])?Config::$db['prefix'].'_':'';
        $this->db = new MysqliDriver(Config::$db['host'], Config::$db['user'], Config::$db['password'], Config::$db['database']);
    }

    public function debug($a) {
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }

}

?>
