<?php

/**
 * @package chahak
 * @version 1.0.0
 */
defined('PMDDA') or die('Restricted access');

class Engine {

    protected $system;

    public function __construct() {
        $this->environmentDetection();
        $this->load();
    }

    public function start() {

        $this->system = new System();
        $this->system->start();
    }

    public function stop() {
        if ($this->system->isTheme()) {
            $this->system->getTheme();
        } else {
            $this->system->getView();
        }
    }

    public function environmentDetection() {
        if (php_sapi_name() == "cli") {
            global $argc, $argv;
            for ($c = 1; $c < $argc; $c++) {
                $param = explode("=", $argv[$c], 2);
                $_GET[$param[0]] = $param[1];
            }
            $_GET['theme'] = 'FALSE';
        }
    }

    public function load() {
        self::loadConfig();
        spl_autoload_register('self::autoloadSystem');
        spl_autoload_register('self::autoloadController');
        spl_autoload_register('self::autoloadModel');
    }

    public static function loadConfig() {
       
    }

    public static function autoloadSystem($class) {

        $fileWithPath = dirname(__FILE__) . '/' . $class . '.php';
        self::includes($fileWithPath);
    }

    public static function autoloadController($class) {
        $fileWithPath = Config::$application . '/application/controllers/' . str_replace('Controller', '', $class) . '.php';
        self::includes($fileWithPath);
    }

    public static function autoloadModel($class) {
        $fileWithPath = Config::$application . '/application/models/' . $class . '.php';
        self::includes($fileWithPath);
    }

    public static function includes($fileWithPath) {
        if (is_readable($fileWithPath)) {
            require_once ($fileWithPath);
        }
    }

}

?>