<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class allFlipkart extends DuskTestCase {

    public function allFlipkart() {

        $this->browse(function (MyBrowser $browser) {
            // $browser->ensurejQueryIsAvailable();
            //echo "sfdsfsfdfdfdf";

            $Select1 = DB::select('select * from scraping_product ');

            // $url1 = $scraping->amazon_url;
            foreach ($Select1 as $selectID) {
                $lastID = $selectID->product_id;
                $paytmmall = $selectID->flipkart_url;
                $browser->visit($paytmmall)
                        ->pause(2000);
                $priceAmazon = $browser->driver->findElement((WebDriverBy::cssSelector('._1vC4OE._37U4_g')))->getText();
                $priceAmazon = str_replace('₹', '', $priceAmazon);
                $priceAmazon = str_replace(',', '', $priceAmazon);
                $verify = DB::select('select count(price_id) AS price from scraping_price WHERE product_id=' . $lastID);
                foreach ($verify as $ver) {
                    $count = $ver->price;
                }
                if ($count == 0) {
                    $update = DB::insert('insert into scraping_price (product_id,flipkart_price) values (?,?)', [$lastID, str_replace(',', '', $priceAmazon)]);
                } else {

                    $IDPrice = DB::select('select price_id  from scraping_price WHERE product_id=' . $lastID);

                    foreach ($IDPrice as $priceID) {
                        $ID = $priceID->price_id;
                    }
                    $countaffected = DB::table('scraping_price')
                            ->where('price_id', $ID)
                            ->update(['flipkart_price' => str_replace(',', '', $priceAmazon)]);
                }
            }
        });
    }

}
