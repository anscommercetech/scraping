<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class myntraDetails extends DuskTestCase {

    public function testWithCss() {
        $this->browse(function (MyBrowser $browser) {

            $url = 'https://www.myntra.com/varanga?userQuery=true';
            $browser->visit($url)
                    ->pause(2000);
            $browser->driver->findElement(WebDriverBy::cssSelector(".results-showmore"))->click();
            $browser->pause(5000);
        });
    }

}
