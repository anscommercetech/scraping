<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class amazonLaravel extends DuskTestCase {

    public function testWithlogin() {

        $this->browse(function (MyBrowser $browser) {
            global $argc, $argv;
            $Select1 = DB::select('select MAX(channel_order_id) AS lastID from ans_channel_order_item ');

            // $url1 = $scraping->amazon_url;
            foreach ($Select1 as $selectID) {
                $lastID = $selectID->lastID;
            }

            $Select2 = DB::select('select p.dimension,coi.channel_order_code,p.product_id from ans_channel_order_item coi JOIN ans_product p ON coi.product_id=p.product_id WHERE coi.channel_order_item_id =' . $lastID);
            foreach ($Select2 as $selectProduct) {
//                $orderID = $selectProduct->channel_order_code;
                $dimension = $selectProduct->dimension;
                $pid = $selectProduct->product_id;
            }
            $dimensionJson = json_decode($dimension);
            $weightJson = $dimensionJson->weight;
            $lengthJson = $dimensionJson->length;
            $widthJson = $dimensionJson->width;
            $heightJson = $dimensionJson->height;

            $browser->visit('https://sellercentral.amazon.in/easyship/schedule?_encoding=UTF8&orderID=' . $argv[4])
                    ->pause(3000)
                    ->type('email', 'amazon@secretwish.in')
                    ->pause(3000)
                    ->type('password', 'secretwish@40')
            ->driver->findElement(WebDriverBy::id('signInSubmit'))->click();
            $browser->pause(3000);
            $weight = $browser->driver->findElement(WebDriverBy::id('pkg_weight'))->getAttribute("value");
            date_default_timezone_set('Asia/Kolkata');
            $browser->pause(3000);
//            $weight = $browser->driver->findElement(WebDriverBy::id('pkg_weight'))->sendKeys($weightJson);
//            $weight = $browser->driver->findElement(WebDriverBy::id('pkg_weight'))->getAttribute("value");
//            $browser->pause(3000);
//            $length = $browser->driver->findElement(WebDriverBy::id('pkg_length'))->clear();
//            $browser->pause(3000);
//            $length = $browser->driver->findElement(WebDriverBy::id('pkg_length'))->sendKeys($lengthJson);
            $length = $browser->driver->findElement(WebDriverBy::id('pkg_length'))->getAttribute("value");
            $browser->pause(3000);
//            $width = $browser->driver->findElement(WebDriverBy::id('pkg_width'))->clear();
//            $browser->pause(3000);
//            $width = $browser->driver->findElement(WebDriverBy::id('pkg_width'))->sendKeys($widthJson);
            $width = $browser->driver->findElement(WebDriverBy::id('pkg_width'))->getAttribute("value");
            $browser->pause(3000);
            $height = $browser->driver->findElement(WebDriverBy::id('pkg_height'))->getAttribute("value");
//            $browser->pause(3000);
//            $height = $browser->driver->findElement(WebDriverBy::id('pkg_height'))->sendKeys($heightJson);
            $newDimension = new \stdClass();
            $newDimension->size = 0;
            $newDimension->weight = $weight;
            $newDimension->length = $length;
            $newDimension->width = $width;
            $newDimension->height = $height;
            $newDimnesionJson = json_encode($newDimension);
            $date = (new \DateTime())->format('Y-m-d H:i:s');
            $countaffected = DB::table('ans_product')
                    ->where('product_id', $pid)
                    ->update(['dimension' => $newDimnesionJson, 'updated' => $date]);

            $price = $browser->driver->findElement(WebDriverBy::cssSelector('.a-spacing-top-micro.a-size-small.a-color-price.a-text-bold'))->getText();
            $browser->pause(3000);
            $browser->driver->findElement(WebDriverBy::id('getPickupSlotsNFees'))->click();
            $browser->pause(3000);
            $browser->driver->findElement(WebDriverBy::id('pickupSchedule'))->click();
            $browser->pause(3000);
        });
    }

}
