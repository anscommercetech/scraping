<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class ExampleTest extends DuskTestCase {

    public function testWithCss() {

        $this->browse(function (MyBrowser $browser) {
            //$orderId = readline("Enter Order id: ");
            global $argc, $argv;
//            for ($c = 1; $c < $argc; $c++) {
//                readline($argv[$c]);
//            }
            // 403-2966189-5649167
            $url = 'https://sellercentral.amazon.in/gp/orders-v2/tax-invoice-wrapper?ie=UTF8&orderID=' . $argv[4];
//            $url = 'https://sellercentral.amazon.in/gp/orders-v2/tax-invoice-wrapper?ie=UTF8&orderID=' . $orderId;
            $browser->visit($url)
                    ->pause(3000)
                    ->type('email', 'amazon@secretwish.in')
                    ->pause(3000)
                    ->type('password', 'secretwish@40')
            ->driver->findElement(WebDriverBy::id('signInSubmit'))->click()

            ;
            $browser->pause(3000);

            $browser->driver->manage()->window()->maximize();

            $browser->pause(3000);

            $browser->driver->executeScript('window.scrollTo(0,115);');
            //$screenshot_of_element = $browser->driver->findElement(WebDriverBy::id("plugin"));
            $browser->driver->takeScreenshot('/home/madhav/Pictures/invoice.png');
        });
    }

}
