<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class allJabong extends DuskTestCase {

    public function allJabong() {

        $this->browse(function (MyBrowser $browser) {
            // $browser->ensurejQueryIsAvailable();
            //echo "sfdsfsfdfdfdf";

            $Select1 = DB::select('select * from scraping_product ');

            // $url1 = $scraping->amazon_url;
            foreach ($Select1 as $selectID) {
                $lastID = $selectID->product_id;
                $paytmmall = $selectID->jabong_url;
                $browser->visit($paytmmall)
                        ->pause(2000);
                $priceAmazon = $browser->driver->findElement((WebDriverBy::className('actual-price')))->getText();
                $priceAmazon = str_replace('₹', '', $priceAmazon);
                $priceAmazon = str_replace(',', '', $priceAmazon);
                $verify = DB::select('select count(price_id) AS price from scraping_price WHERE product_id=' . $lastID);
                foreach ($verify as $ver) {
                    $count = $ver->price;
                }
                if ($count == 0) {
                    $update = DB::insert('insert into scraping_price (product_id,jabong_price) values (?,?)', [$lastID, str_replace(',', '', $priceAmazon)]);
                } else {

                    $IDPrice = DB::select('select price_id  from scraping_price WHERE product_id=' . $lastID);

                    foreach ($IDPrice as $priceID) {
                        $ID = $priceID->price_id;
                    }
                    $countaffected = DB::table('scraping_price')
                            ->where('price_id', $ID)
                            ->update(['jabong_price' => str_replace(',', '', $priceAmazon)]);
                }
            }
        });
    }

}
