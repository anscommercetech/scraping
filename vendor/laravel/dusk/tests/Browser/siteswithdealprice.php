<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class allSitesTest extends DuskTestCase {

    public function testWithCss() {

        $this->browse(function (MyBrowser $browser) {
            // $browser->ensurejQueryIsAvailable();
            //echo "sfdsfsfdfdfdf";
             $Select1 = DB::select('select MAX(product_id) AS lastID from scraping_product ');

            // $url1 = $scraping->amazon_url;
            foreach ($Select1 as $selectID) {
                $lastID = $selectID->lastID;
            }
            $scrapings = DB::select('select product_id,amazon_url,flipkart_url,myntra_url,jabong_url,paytmmall_url from scraping_product ');
            foreach($scrapings as $scraping) {
                $url1 = $scraping->amazon_url;
              
                $url2 = $scraping->flipkart_url;
                $url3 = $scraping->myntra_url;
                $url4 = $scraping->jabong_url;
                $url5 = $scraping->paytmmall_url;
            }
                if (!empty($url1)) {
                    $browser->visit($url1)
                            ->pause(2000);
                    $text = $browser->driver->findElement(WebDriverBy::id('priceblock_dealprice'))->getText();
                    $browser->pause(2000);
                }
                if (!empty($url2)) {
                    $browser->visit($url2);
                    $browser->pause(2000);
                    $price2 = $browser->driver->findElement(WebDriverBy::cssSelector('._1vC4OE._37U4_g'))->getText();
                    $price2 = str_replace('₹', '', $price2);
                    $price2 = str_replace(',', '', $price2);
                    $browser->pause(2000);
                }
                if(!empty($url3))
                {
                $browser->visit($url3);
                $browser->pause(2000);
                $price3 = $browser->driver->findElement(WebDriverBy::className('pdp-price'))->getText();
                $price3 = str_replace('Rs. ', '', $price3);
                $price3 = str_replace('', '', $price3);
                $browser->pause(2000);
                }
                if(!empty($url4))
                {
                $browser->visit($url4);
                $browser->pause(2000);
                $price4 = $browser->driver->findElement(WebDriverBy::className('actual-price'))->getText();
                $browser->pause(2000);
                }
                if(!empty($url5))
                {
                $browser->visit($url5);
                $browser->pause(2000);
                $price5 = $browser->driver->findElement(WebDriverBy::className('_1V3w'))->getText();
                $price5 = str_replace('₹', '', $price5);
                $price5 = str_replace(',', '', $price5);
                }


                $update = DB::insert('insert into scraping_price (product_id,amazon_price,flipkart_price,myntra_price,jabong_price,paytmmall_price) values (?,?,?,?,?,?)', [$lastID, str_replace(',', '', $text), $price2, $price3, $price4, $price5]);

            
        });
    }
}