<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class amazonPrint extends DuskTestCase {

    public function testWithprint() {

        $this->browse(function (MyBrowser $browser) {
            global $argc, $argv;
            $browser->visit('https://sellercentral.amazon.in/hz/orders/details?_encoding=UTF8&orderId=' . $argv[4])
                    ->pause(3000)
                    ->type('email', 'amazon@secretwish.in')
                    ->pause(3000)
                    ->type('password', 'secretwish@40')
            ->driver->findElement(WebDriverBy::id('signInSubmit'))->click();
            $browser->pause(3000);
            $trackingID = $browser->driver->executeScript("return document.getElementsByClassName('a-link-normal')[4].text");
            echo $trackingID;
            
            $browser->driver->findElement(WebDriverBy::id('myo-print-tax-invoice-button'))->click();
            $browser->pause(3000);

            $window1 = $browser->driver->getWindowHandles();
            $browser->driver->switchTo()->window(end($window1));
            $browser->pause(3000);
            $browser->driver->manage()->window()->maximize();
            $browser->pause(3000);
            $browser->driver->executeScript('window.scrollTo(0,112);');
            //$screenshot_of_element = $browser->driver->findElement(WebDriverBy::id("plugin"));
            $browser->driver->takeScreenshot('/home/madhav/Pictures/tax_invoice.png');
            $browser->driver->close();
            $browser->pause(3000);
            $window2 = $browser->driver->getWindowHandles();
            $browser->driver->switchTo()->window(end($window2));
            $browser->pause(3000);
            $browser->driver->findElement(WebDriverBy::id('a-autoid-3-announce'))->click();
            $browser->pause(3000);
            $browser->driver->findElement(WebDriverBy::id('printLabel'))->click();
            $browser->pause(3000);
            $window3 = $browser->driver->getWindowHandles();
            $browser->driver->switchTo()->window(end($window3));
            $browser->pause(3000);
            $browser->driver->manage()->window()->maximize();
            $browser->pause(3000);
            $browser->driver->takeScreenshot('/home/madhav/Pictures/label.png');
        });
    }

}
