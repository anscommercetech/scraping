<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class allAmazon extends DuskTestCase {

    public function testWithCss() {

        $this->browse(function (MyBrowser $browser) {
            // $browser->ensurejQueryIsAvailable();
            //echo "sfdsfsfdfdfdf";

            $Select1 = DB::select('select * from scraping_product where product_id >=2498 ');

            // $url1 = $scraping->amazon_url;
            foreach ($Select1 as $selectID) {
                $lastID = $selectID->product_id;
                $paytmmall = $selectID->amazon_url;
                $browser->visit($paytmmall)
                        ->pause(1000);
                if (count($browser->driver->findElements(WebDriverBy::cssSelector(".a-span12.a-color-secondary.a-size-base"))) != 0) {

                    $oldPrice = $browser->driver->findElement(WebDriverBy::cssSelector(".a-span12.a-color-secondary.a-size-base"))->getText();
                    $oldPrice = str_replace('₹', '', $oldPrice);
                    $oldPrice = str_replace(',', '', $oldPrice);
                } else {
                    $oldPrice = NULL;
                }
                if ($browser->driver->findElements(WebDriverBy::id('priceblock_ourprice'))) {
                    $priceAmazon = $browser->driver->findElement(WebDriverBy::id('priceblock_ourprice'))->getText();
                } elseif ($browser->driver->findElements(WebDriverBy::id('priceblock_dealprice'))) {
                    $priceAmazon = $browser->driver->findElement(WebDriverBy::id('priceblock_dealprice'))->getText();
                } elseif ($browser->driver->findElements(WebDriverBy::id('priceblock_saleprice'))) {
                    $priceAmazon = $browser->driver->findElement(WebDriverBy::id('priceblock_saleprice'))->getText();
                } else {
                    $priceAmazon = NULL;
                }
                $priceAmazon = str_replace('₹', '', $priceAmazon);
                $priceAmazon = str_replace(',', '', $priceAmazon);

                $update = DB::insert('insert into scraping_price (product_id,amazon_price,flipkart_price) values (?,?,?)', [$lastID, str_replace(',', '', $priceAmazon), $oldPrice]);
            }
        });
    }

}
