<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class paytmscrapingTest extends DuskTestCase {

    public function testWithCss() {

        $this->browse(function (MyBrowser $browser) {
            
            
            $url = 'http://localhost:9515';

            $uri = '/session/' . $browser->driver->getSessionID() . '/chromium/send_command';
            $body = [
                'cmd' => 'Page.setDownloadBehavior',
                'params' => ['behavior' => 'allow', 'downloadPath' => '/var/www/html/madhav/']
            ];
            (new \GuzzleHttp\Client())->post($url . $uri, ['body' => json_encode($body)]);
            
            $url = 'https://seller.paytm.com/login';
            
            $browser->visit($url)
                    ->pause(2000)
                    ->type('username', 'sushant@secretwish.in')
                    ->pause(2000)
                    ->type('password', 'paytm@2017')
            ->driver->findElement(WebDriverBy::cssSelector('.waves-effect.waves-light.btn-large.white-text.ng-binding'))->click()
            ;
            $browser->pause(8000);
            $browser->visit('https://seller.paytm.com/new/return_new/after_delivery');

            $browser->pause(3000);

            $browser->driver->findElement(WebDriverBy::cssSelector(".dropdown-button.menu.btn.white-text"))->click();
            $browser->pause(6000);
        });
    }

}
