<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use App\Testing\MyBrowser;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Facades\DB;

// php artisan dusk ---  to run

class myntraDetails extends DuskTestCase {

    public function testWithCss() {
        $this->browse(function (MyBrowser $browser) {

            $url = 'https://www.myntra.com/kurta-sets/varanga/varanga-black-printed-kurta-with-palazzo-trousers/1730085/buy';
            $browser->visit($url)
                    ->pause(2000);
            $title = $browser->driver->findElement(WebDriverBy::cssSelector(".pdp-title"))->getText();
            $browser->pause(2000);
            $oldPrice = $browser->driver->findElement(WebDriverBy::cssSelector(".pdp-discount-container"))->getText();
            $browser->pause(2000);

            $discountPrice = $browser->driver->findElement(WebDriverBy::cssSelector(".pdp-selling-price"))->getText();
            $browser->pause(2000);

            $size = $browser->driver->findElement(WebDriverBy::cssSelector(".size-buttons-size-buttons"))->getText();
            $browser->pause(2000);

            $productDetails = $browser->driver->findElement(WebDriverBy::cssSelector(".pdp-productDescriptorsContainer"))->getText();
            $browser->pause(2000);

            $productCode = $browser->driver->findElement(WebDriverBy::cssSelector(".supplier-product-code"))->getText();
            $browser->pause(2000);
            echo $productCode . '\n';

            echo $title . "\n";
            echo $oldPrice . "\n";
            echo $discountPrice . "\n";
            echo $size . "\n";
            echo $productDetails . '\n';
        });
    }

}
