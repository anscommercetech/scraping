<?php

namespace App\Testing;

use Laravel\Dusk\ElementResolver;
use Facebook\WebDriver\WebDriverBy;

class DynamicElementResolver extends ElementResolver
{  
    /**
     * Find an element by the given selector or throw an exception.
     *
     * @param  string  $selector
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    public function findOrFail($selector)
    {
        return $this->driver->findElement(
            (is_string($selector)) ? WebDriverBy::cssSelector($this->format($selector)) : $selector
        );
    }

    /**
     * Find the elements by the given selector or return an empty array.
     *
     * @param  string  $selector
     * @return array
     */
    public function all($selector)
    {
        try {
            return $this->driver->findElements(
                (is_string($selector)) ? WebDriverBy::cssSelector($this->format($selector)) : $selector
            );
        } catch (Exception $e) {
            //
        }

        return [];
    }
}