<?php

namespace App\Testing;

use Laravel\Dusk\Browser;

class MyBrowser extends Browser {

    /**
     * Create a browser instance.
     *
     * @param  \Facebook\WebDriver\Remote\RemoteWebDriver  $driver
     * @param  ElementResolver  $resolver
     * @return void
     */
    public function __construct($driver, $resolver = null)
    {
        $this->driver = $driver;

        $this->resolver = $resolver ?: new DynamicElementResolver($driver);
    }

    /**
     * Find an element by the given selector or return null.
     *
     * @param  string|\Facebook\WebDriver\WebDriverBy  $selector
     * @return \Facebook\WebDriver\Remote\RemoteWebElement|null
     */
    public function findBySelector($selector) {
        return $this->resolver->find($selector);
    }

}