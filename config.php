<?php

/**
 * @author Nanhe Kumar <nanhe.kumar@gmail.com>
 * @version 1.0.0
 * @package chahak
 */

class Config {

    public static $application = '/var/www/html/ans_scraping';

    public static $theme = 'gentelella';
    public static $autocomplete=false;
    public static $language = array(
        'english' => 'English',
        'german' => 'German',
    );
    public static $server=array(
        'name' => "Localhost",
        'server'=>false,
        'host' => "127.0.0.1",
        'port'=>"27017",
        'timeout'=>0,
    );
    public static $authentication = array(
        'authentication'=>false,
        'user' => 'admin',
        'password' => 'admin'
    );
    public static $authorization = array(
        'readonly'=>false,
    );

    /**
     *
     * @var array
     * @link http://in2.php.net/manual/en/mongoclient.construct.php (for more detail)
     */
    public static $connection = array(
        'server' => "", //mongodb://localhost:27017
        'options' => array(
            'replicaSet' => false,
        ), //
    );
    
     public static $db=array(
        'host' => "localhost",
        'user' => "root",
        "password" => "Madhav91",
        "database" =>"ans_scraping",
        "prefix"=>'scraping'

    );
    public static $cli = array(
        'Index/Cli',
        
        'Opencart/SyncCatalog',//php index.php load=Opencart/SyncCatalog channel_id=1
        'ChannelProductLog/Sync',//php index.php load=ChannelProductLog/Sync

        'Opencart/SyncOrders', //php index.php load=Opencart/SyncOrders channel_id=1
        'ChannelOrderLog/Sync', //php index.php load=ChannelOrderLog/Sync
        
        'Cron/ProcessLinkedProduct', //php index.php load=Cron/ProcessLinkedProduct channel_id=1 
        'Cron/ProcessFulfillable', //php index.php load=Cron/ProcessFulfillable
        'Cron/ProcessShippingPackage',//php index.php load=Cron/ProcessShippingPackage
        
    );
}

?>
