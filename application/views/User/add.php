<?php
$users = $this->data['getDataUsers']
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>AddUser <small>Home > user </small></h2>
                <div class="hidden-sm hidden-xs pull-right">
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <form id="form-user" data-parsley-validate class="form-horizontal form-label-left" method="post" action="index.php">                   
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required" style="color:RED">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input  class="form-control" type="text" id="name" name="name" value="<?php echo isset($users['name']) ? $users['name'] : ""; ?>" placeholder="Enter Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required" style="color:RED">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" type="text" id="email" name="email" value="<?php echo isset($users['email']) ? $users['email'] : ""; ?>" placeholder="Enter Email" required>
                        </div>
                    </div>
                    
                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required" style="color:RED">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  class="form-control" type="password" id="password" name="password" value="" placeholder="*********" required >
                            </div>
                        </div>
                 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Status <span class="required" style="color:RED">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="status" name="status" class="form-control" required>

                                        <option value="1"  <?php
                                        if (isset($users['status'])) {
                                            if ($users['status'] == 1) {
                                                echo 'selected';
                                            }
                                        }
                                        ?>>Enabled</option>
                                        <option value="0"  <?php
                                        if (isset($users['status'])) {
                                            if ($users['status'] == 0) {
                                                echo 'selected';
                                            }
                                        }
                                        ?>>Disabled</option>
                                    </select>
                                </div>
                            
                            <div class="clearfix"></div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <input type="hidden" name="user_id" value="<?php echo $users["user_id"]; ?>"/>
                                    <input type="hidden" name="load" value="<?php echo $this->data["load"]; ?>"/>
                            <button type="submit" class="btn btn-success" name="buttonSave">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>