<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>User <small>Home > User </small></h2>
                <div class="hidden-sm hidden-xs pull-right">
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>                    
                    <a class="btn btn-primary" href="<?php echo Theme::URL('User/Add'); ?>" title="Add User"><i class="fa fa-plus"></i> Add User</a>
                </div>
                <div class="visible-sm visible-xs pull-right">
                    <a class="btn btn-primary" href="<?php echo Theme::URL('Category/Add'); ?>" title="Add Category"><i class="fa fa-plus"></i></a>
                </div>                              
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>User List</p>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th class="column-title">Id </th>
                                <th class="column-title">Name </th>
                                <th class="column-title">Email </th>       
                                <th class="column-title">Added On </th>
                                <th class="column-title">Last Updated On </th>
                                <th class="column-title">Status </th>                    
                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                </th>
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($this->data['users'] as $user) { ?>
                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records">
                                    </td>
                                    <td class=" "><?php echo $user['user_id']; ?></td>
                                    <td class=" "><?php echo $user['name']; ?></td>   
                                    <td class=" "><?php echo $user['email']; ?></td>
                                    <td class=" "><?php echo $user['created']; ?></td>
                                    <td class=" "><?php echo $user['updated']; ?></td>
                                    <td class=" "><?php echo $user['status'] == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>'; ?></td>
                                    <td class=" last">
                                        <a href="<?php echo Theme::URL('User/Edit', array('user_id' => $user['user_id'])); ?>" title="Edit Category"><i class="fa fa-pencil"></i></a>
                                        &nbsp;
                                        <a href="<?php echo Theme::URL('User/Delete', array('user_id' => $user['user_id'])); ?>" title="Delete Category"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Theme::pagination($this->data['total']); ?>