<style type="text/css">
    .indexHide, .left_col, footer{
        display: none;
    }
    .nav-md .container.body .col-md-3.left_col{display: none;}
    .nav-md .container.body .right_col {
        margin-left: 0;
    }
    body {
    background: #f7f7f7;
    }

</style>

<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content login-wrap">
            <form id="form-login" method="post" action="index.php">
                <div class="login-logo">
                     <img src="http://anscommerce.com/wp-content/uploads/2017/11/cropped-ans.logo_.png" alt="" class="brand-logo">
                </div>
                <div class="panel">
                    <div>
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="Email" required=""  name="email" id="email"/>
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Password" required="" name="password" id="password" />
                    </div>
                    <div>
                        <input type="submit" name="buttonLogin" value="Log in" class="btn btn-default submit" />
                        <a class="reset_pass" href="#" style="display: none;">Lost your password?</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                

                <div class="separator">
                    <div>                      
                        <p> Powered By <a href= "http://anscommerce.com/"> ANS Commerce </a></p>
                    </div>
                </div>
                
                <input type="hidden" name="load" value="Login/Index"/>
            </form>
        </section>
    </div>


</div>