<?php $getData = $this->data['getallData']; ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Categories <small>Home > Categories </small></h2>
                <div class="hidden-sm hidden-xs pull-right">
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-category" method="post" action="index.php" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product SKU<span class="required" style="color:#ff0000">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="product_sku" id="name"  required class="form-control col-md-7 col-xs-12" value="<?php if (!empty($getData['product_sku'])) {
    echo $getData['product_sku'];
} else { echo '';}
?>">

                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Amazon URL
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="amazon_url" id="name"  class="form-control col-md-7 col-xs-12" value="<?php if(isset($getData['amazon_url'])) {
                            echo $getData['amazon_url'];} ?>">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Flipkart URL </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="flipkart_url" id="name" class="form-control col-md-7 col-xs-12" value="<?php if(isset($getData['flipkart_url'])) {
                            echo $getData['flipkart_url'];} ?>">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Myntra URL </label>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="myntra_url" id="name"  class="form-control col-md-7 col-xs-12" value="<?php if(isset($getData['myntra_url'])) {
                            echo $getData['myntra_url'];} ?>">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Jabong URL
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="jabong_url" id="name" class="form-control col-md-7 col-xs-12" value="<?php if(isset($getData['jabong_url'])) {
                            echo $getData['jabong_url'];} ?>">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Paytmmall URL
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="paytmmall_url" id="name"  class="form-control col-md-7 col-xs-12" value="<?php if(isset($getData['paytmmall_url'])) {
                            echo $getData['paytmmall_url'];} ?>">
                        </div>
                    </div>                    

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    <input type="hidden" name="load" value="<?php echo $this->data['load']; ?>"/>
                                        <input type="hidden" name="product_id" value=" <?php if(isset($getData['product_id'])) {
                            echo $getData['product_id'];} ?> "/>

                </form>
            </div>
        </div>
    </div>
</div>