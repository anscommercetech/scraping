<?php
$getAllInventory = $this->data['url'];
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Price Comparison Tool <small>Stores</small></h2>
                <div class="hidden-sm hidden-xs pull-right">
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <a class="btn btn-primary" href="<?php echo Theme::URL('Scraping/import'); ?>" title="Import CSV"><i class="fa fa-upload"></i> Import URL List</a>

                    <a class="btn btn-primary" href="<?php echo Theme::URL('Scraping/Add'); ?>" title="Add URL"><i class="fa fa-plus"></i> Add URL</a>

                </div>
                <div class="visible-sm visible-xs pull-right">
                    <a class="btn btn-primary" href="<?php echo Theme::URL('Scraping/import'); ?>" title="Import CSV"><i class="fa fa-upload"></i></a>

                    <a class="btn btn-primary" href="<?php echo Theme::URL('Scraping/Add'); ?>" title="Add URL"><i class="fa fa-plus"></i></a>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>Stores</p>
                <div class="table-responsive custom-table">
                    <table class="table table1 table-bordered table-striped table-resp">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th class="column-title">Product SKU </th>
                                <th class="column-title">Amazon URL </th>
                                <th class="column-title"> Flipkart URL</th>
                                <th class="column-title">Myntra URL </th>                          
                                <th class="column-title">Jabong URL </th>
                                <th class="column-title">PayTM Mall URL </th>
                                <th class="column-title">Action </th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($getAllInventory as $key => $values) { ?>
                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records">
                                    </td>
                                    <td class=" "><a href="<?php echo Theme::URL('Scraping/priceIndex', array('product_id' => $values['product_id'])) ?>"><?php echo $values['product_sku']; ?></a></td> 
                                    <td class=""> <?php echo $values['amazon_url']; ?></a></td>
                                    <td class=""><?php echo $values['flipkart_url']; ?>  </td>
                                    <td class=" "><?php echo $values['myntra_url']; ?></td>                        
                                    <td class=" "><?php echo $values['jabong_url']; ?></td>                                        
                                    <td class=""><?php echo $values["paytmmall_url"]; ?></td>
                                    <td class=""><a href="<?php echo Theme::URL('Scraping/edit', array('product_id' => $values['product_id'])); ?>"><i class="fa fa-pencil"></i> </a></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <footer>
                    <div class="footer_fixed">
                        <?php Theme::pagination($this->data['total']); ?>
                    </div>
                </footer>
            </div>
        </div>
    </div>
</div>
