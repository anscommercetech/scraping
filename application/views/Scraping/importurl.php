<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Price Comparison <small>URLs> Upload</small></h2>
                <div class="hidden-sm hidden-xs pull-right">
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <a class="btn btn-primary pull-right" href="<?php echo Theme::URL('Scraping/DownloadCSV'); ?>" title="Sample CSV"><i class="fa fa-download"></i> Sample CSV</a>
                </div>
                <div class="visible-sm visible-xs pull-right upload">
                    <a class="btn btn-primary pull-right" href="<?php echo Theme::URL('Scraping/DownloadCSV'); ?>" title="Sample CSV"><i class="fa fa-download"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>Drag multiple files to the box below for multi upload or click to select files. .</p>
                <form action="index.php?load=Scraping/UploadFile&theme=false" class="dropzone"></form>
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
</div>