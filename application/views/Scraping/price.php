<?php
$getAllInventory = $this->data['price'];
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Price Comparison Tool <small>Prices </small></h2>
                   <div class="hidden-sm hidden-xs pull-right">
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                       
                        <a class="btn btn-primary" href="<?php echo Theme::URL('Scraping/download'); ?>" title="Download Price List"><i class="fa fa-download"></i> Download Price List</a>
                    </div>
                    <div class="visible-sm visible-xs pull-right">
                     
                        <a class="btn btn-primary" href="<?php echo Theme::URL('Scraping/download'); ?>" title="Download Price List"><i class="fa fa-download"></i></a>
                    </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>Price List of SKU</p>
                <div class="table-responsive custom-table">
                    <table class="table table1 table-bordered table-striped table-resp">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th class="column-title">Product SKU </th>
                                <th class="column-title">Amazon Price </th>
                                <th class="column-title"> Flipkart Price</th>
                                <th class="column-title">Myntra price </th>                          
                                <th class="column-title">Jabong price </th>
                                <th class="column-title">PayTM Mall price </th>

                            </tr>
                        </thead>
                        <tbody>
                           
                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records">
                                    </td>
                                    <td class=" "><?php if(!EMPTY($getAllInventory['product_sku'])) { echo $getAllInventory['product_sku']; }?></td> 
                                    <td class=""> <?php if(!EMPTY($getAllInventory['amazon_price'])) { echo $getAllInventory['amazon_price']; }?> </a></td>
                                    <td class=""> <?php if(!EMPTY($getAllInventory['flipkart_price'])) { echo $getAllInventory['flipkart_price'];} ?>  </td>
                                    <td class=" "><?php if(!EMPTY($getAllInventory['myntra_price'])) { echo $getAllInventory['myntra_price']; }?></td>                        
                                    <td class=" "><?php if(!EMPTY($getAllInventory['jabong_price'])) { echo $getAllInventory['jabong_price']; } ?></td>                                        
                                    <td class=""><?php if(!EMPTY($getAllInventory['paytmmall_price'])) { echo $getAllInventory["paytmmall_price"];} ?></td>
                                </tr>
                            
                        </tbody>
                    </table>
                </div>
                <footer>
                    <div class="footer_fixed">
                    </div>
                </footer>
            </div>
        </div>
    </div>
</div>
