<?php defined('PMDDA') or die('Restricted access'); ?>
<?php

class User extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function getUser($data = array()) {
        $sql = "SELECT * FROM " . $this->prefix ."user WHERE email='" . $this->db->escape($data['email']) . "' AND password = '" . md5($data['password']) . "'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getUsers($data = array()) {
        if (!isset($data['start'])) {
            $data['start'] = 0;
        }
        if (!isset($data['limit'])) {
            $data['limit'] = 20;
        }
        $sql = "SELECT * FROM " . $this->prefix . "user ";
        $sql .= " LIMIT " . $data['start'] . "," . $data['limit'];
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalUsers() {
        $sql = "SELECT count(*) AS total FROM " . $this->prefix . "user WHERE 1";
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getUsersdata($data = array()) {
        $sql = "SELECT u.user_id,u.name,u.email,u.status,u.password FROM " . $this->prefix . "user u where u.user_id=" . $data['user_id'];
        $query = $this->db->query($sql);
        $result = $query->row;
        return $result;
    }

    public function AddUser($data = array()) {
        $date = date('Y-m-d h:i:s');
        $sql = "INSERT INTO pm_user("
                . "name,"
                . "email,"
                . "password,"
                . "created,"
                . "updated,"
                . "status"
                . ") VALUES ("
                . "'" .$this->db->escape($data['name']) . "',"
                . "'" . $this->db->escape($data['email']) . "',"
                . "'" . $this->db->escape(md5($data['password'])) . "',"
                . "'" . $date . "',"
                . "'" . $date . "',"
                . "'" . $this->db->escape($data['status']) . "'"
                . ")";

        $query = $this->db->query($sql);
        $lastid = $this->db->getLastId();
        return $lastid;
    }

    public function EditUser($data = array()) {
        $sql = "UPDATE pm_user SET ";
        if ($data['name'] != NULL || $data['name'] != "") {
            $sql .= " name = '" . $this->db->escape(($data['name'])) . "',";
        }
          if ($data['email'] != NULL || $data['email'] != "") {
            $sql .= " email = '" . $this->db->escape(($data['email'])) . "',";
        }
          if ($data['password'] != NULL || $data['password'] != "") {
            $sql .= " password = '" . $this->db->escape((md5($data['password']))) . "',";
        }
          if ($data['status'] != NULL || $data['status'] != "") {
            $sql .= " status = '" . $this->db->escape(($data['status'])) . "'";
        }
            $sql .= " WHERE user_id = " . $data['user_id'] . "";
            $query = $this->db->query($sql);
            $result = $this->db->countAffected();
            return $result;
        }
        
    

    public function InactiveUser($data = array()) {
        $sql = "UPDATE hr_user SET status=0";
        $sql .= " WHERE user_id = " . $data['user_id'] . "";
        $query = $this->db->query($sql);

        $result = $this->db->countAffected();
        return $result;
    }

}
