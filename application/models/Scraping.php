<?php

class Scraping extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function urlList($data = array()) {
        $sql = "SELECT * FROM " . $this->prefix . "product  WHERE 1";
        $query = $this->db->query($sql);
        return $query->rows;
    }
     public function price($data = array()) {
    $sql = "SELECT * FROM " . $this->prefix . "product pr JOIN " .$this->prefix."price p ON p.product_id=pr.product_id WHERE 1 and p.product_id=".$data['product_id'];
        $query = $this->db->query($sql);
        return $query->row;
    }
    
     public function priceList($data = array()) {
    $sql = "SELECT * FROM " . $this->prefix . "product pr JOIN " .$this->prefix."price p ON p.product_id=pr.product_id WHERE 1 ";
        $query = $this->db->query($sql);
        return $query->rows;
    }
    public function getTotalurl($data = array()) {
        if (!isset($data['start'])) {
            $data['start'] = 0;
        }
        if (!isset($data['limit'])) {
            $data['limit'] = 20;
        }
        $sql = "SELECT count(*) as total FROM " . $this->prefix . "product ";
        $sql .= " LIMIT " . $data['start'] . "," . $data['limit'];
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function add($data = array()) {
        $sql = "INSERT INTO `".$this->prefix."product` ( `product_sku`,`amazon_url`, `flipkart_url`,`myntra_url`, `jabong_url`,`paytmmall_url`,`status`,`created`) VALUES ( '".$this->db->escape($data
                ['product_sku'])."', '" . $this->db->escape($data['amazon_url']) . "', '" .$this->db->escape($data['flipkart_url'])  . "', '" . $this->db->escape($data['myntra_url'])  . "', '" . $this->db->escape($data['jabong_url'] ) . "', '" .$this->db->escape($data['paytmmall_url'])  . "', '" .$this->db->escape(1) . "', NOW())";
        $query = $this->db->query($sql);
        return $this->db->getLastId();
        
    }
    
      public function getData($data = array()) {
        $sql = "SELECT * FROM " . $this->prefix . "product WHERE 1 AND product_id =".$data['product_id'];
        $query = $this->db->query($sql);
        return $query->row;
    }
    
    public function edit($data=array()) {
        $sql = "UPDATE `".$this->prefix."product` SET  `product_sku`='".$this->db->escape($data
                ['product_sku'])."',`amazon_url`='".$this->db->escape($data['amazon_url'])."', `flipkart_url`='".$this->db->escape($data['flipkart_url'])."',`myntra_url`='". $this->db->escape($data['myntra_url']) ."', `jabong_url`='". $this->db->escape($data['jabong_url'] )."',`paytmmall_url`='".$this->db->escape($data['paytmmall_url']) ."',`status`=".$this->db->escape(1).",`created`=NOW() WHERE product_id=".$data['product_id'];
        $query = $this->db->query($sql);
        return $this->db->getLastId();
    }
     
public function download($data = array()) {
        require_once Config::$application . '/vendor/PHPExcel.php';
        

        $heading = array("Product SKu", "Amazon Price", "Flipkart Price", "Myntra Price", "Jabong Price", "Paytmmall Price");
        $i = 1;
        ini_set('memory_limit', '2048M');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $heading[0]);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $heading[1]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $heading[2]);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $heading[3]);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $heading[4]);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $heading[5]);
        $i++;
                $getAllInventory = $this->priceList();

        foreach ($getAllInventory as $key => $values) {
            if ($values['product_sku'] != "" || $values['product_sku'] != NULL) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $values['product_sku']);
            }
            if ($values['amazon_price'] != "" || $values['amazon_price'] != NULL) {
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $values['amazon_price']);
            }
            if ($values['flipkart_price'] != "" || $values['flipkart_price'] != NULL) {
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $values['flipkart_price']);
            }
            if ($values['myntra_price'] != "" || $values['myntra_price'] != NULL) {
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $values['myntra_price']);
            }
            if ($values['jabong_price'] != "" || $values['jabong_price'] != NULL) {
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $values['jabong_price']);
            }
            if ($values['paytmmall_price'] != "" || $values['paytmmall_price'] != NULL) {
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $values['paytmmall_price']);
            }
            $i++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('Price List');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        unset($objPHPExcel);
        // We'll be outputting an excel file
        if (isset($data['job_id'])) {
            $objWriter->save($pathname . '/till_' . $limit . '.xls');
            return 0;
        } else {
            unset($getAllInventory);
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Price List.xls"');
            $objWriter->save('php://output');
        }
        unset($objWriter);
        exit;
    }
}
