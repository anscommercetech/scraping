<?php

/**
 * @package ANS Commerce
 * @version 1.0.0
 * @link http://anscommerce.com/
 * @author Ankit Jain <ankit.jain@anscommerce.com> , Madhav Kurup
 */
defined('PMDDA') or die('Restricted access');

class UserController extends Controller {

    public function Index() {
        $start = $this->request->getParam('start', 0);
        $limit = $this->request->getParam('limit', 20);
        $model = new User();
        $data = array(
            'total' => $model->getTotalUsers(),
            'users' => $model->getUsers(array('start' => $start, 'limit' => $limit)),
        );
        
        $this->display('index', $data);
    }

    public function Add() {
        $data = array();
        $model = new User();
         $data['getDataUsers']=array();
        $dataUser=array();
        if($this->request->isPost()&&$this->validation()){
            $dataUser=$this->request->post();
            $model->AddUser($dataUser);
            $this->message->sucess = I18n::t('User added');
            $this->request->redirect(Theme::URL('User/Index'));
                    
        }
        $data["load"]="User/Add";
        $this->display('add', $data);

    }
    public function Edit(){
        $data = array();
        $model = new User();
        $dataUser=array();
        $data['getDataUsers']=$model->getUsersdata(array('user_id'=>$this->request->getParam('user_id')));
        if($this->request->isPost()&&$this->validation()){
            $dataUser=$this->request->post();
            $model->EditUser($dataUser);
            $this->message->sucess = I18n::t('User details updated');
            $this->request->redirect(Theme::URL('User/Index'));
                    
        }
        $data["load"]="User/Edit";
        $this->display('add', $data);
    }
    public function validation(){
        if (!filter_var($this->request->getPost('email'), FILTER_VALIDATE_EMAIL)) {
            $this->message->error = I18n::t('Invalid Email ID');return FALSE;
        }
      
        return TRUE;
    }
}
