<?php

class ScrapingController extends Controller {

    public function urlIndex() {
        $start = $this->request->getParam('start', 0);
        $limit = $this->request->getParam('limit', 20);
        $model = new Scraping();
        $data = array(
            'total' => $model->getTotalurl(),
            'url' => $model->urlList(array('start' => $start, 'limit' => $limit)),
        );
        $this->display('scraping_url', $data);
    }

    public function add() {
        $model = new Scraping();
        if($this->request->isPost())
        {
            $dataScrape= $this->request->post();
            $model->add($dataScrape);
            $this->message->sucess = I18n::t('URL added');
            $this->request->redirect(Theme::URL('Scraping/urlIndex'));
        }
        $data = array();
        $data['getallData'] = array();

        $data['load'] = 'Scraping/add';
        $this->display('add', $data);
    }
    
    public function edit() {
        $data=array();
       $model = new Scraping();
       if($this->request->isPost())
       {
        $model->edit($this->request->post());  
        $this->message->sucess = I18n::t('URL added');
            $this->request->redirect(Theme::URL('Scraping/urlIndex'));
       }
        $data['getallData']=$model->getData(array('product_id'=> $this->request->getParam('product_id')));
        $data['load']='Scraping/edit';
        $this->display('add',$data);
    }
    public function priceIndex() {
         
        $model = new Scraping();
        $data['price'] =$model->price(array('product_id'=> $this->request->getParam('product_id')));
        
        $this->display('price', $data);
    }
    public function import(){
        $data = array();
        $this->display('importurl', $data);
    }

    public function DownloadCSV() {
        $file = Config::$application . '/application/upload/sample/url.csv';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function UploadFile() {
        $session = Application::getInstance('Session');
        $this->user = $session->user;
        $targetDir = Config::$application . '/application/upload/url/';
        if (!file_exists($targetDir)) {
            mkdir($targetDir, 777);
        }
        $userId = $this->user['user_id'] . '_';
        $fileName = trim(addslashes($_FILES['file']['name']));
        $fileName = str_replace(' ', '_', $fileName);
        $fileName = preg_replace('/\s+/', '_', $fileName);
        $fileName = $userId . date('Y_m_d_h_i_s_') . $fileName;
        $targetFile = $targetDir . $fileName;

        $imageFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
       
        if ($imageFileType == 'csv') {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
                $this->CSV($targetFile);
            } else {
                exit("Sorry, there was an error uploading your file.");
            }
        }
    }

    protected function validate($data = array()) {
        return true;
    }

    protected function CSV($fileName) {
        $count = 0;
        $model = new Scraping();
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $rowData = array(
                    'product_sku' => isset($data[0]) ? $data[0] : NULL,
                    'amazon_url' => isset($data[1]) ? $data[1] : NULL,
                    'flipkart_url' => isset($data[2]) ? $data[2] : NULL,
                    'myntra_url' => isset($data[3]) ? trim($data[3]) : NULL,
                    'jabong_url' => isset($data[4]) ?($data[4]) : NULL,
                    'paytmmall_url' => isset($data[5]) ? $data[5] : NULL,
                    
                    'file_name' => $fileName,
                );
                if($count!=0) {
                $lastId = $model->add($rowData);
                        if(!$lastId){
                            echo "Problem Inserting Data In Category Through File";
                            $this->debug($rowData);
                        }
                }
                $count++;
            }
            fclose($handle);
        }
    }
    public function download($data = array()) {
        if(!isset($data)){
            $data = array();
        }
        $model = new Scraping();
        $message = $model->download($data);
        $this->message->sucess = I18n::t($message);
        $this->request->redirect(Theme::URL('Inventory/Index'));
    }
}
