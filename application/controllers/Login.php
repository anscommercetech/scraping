<?php

/**
 * @package PHPmongoDB
 * @version 1.0.0
 * @link http://www.phpmongodb.org
 */
defined('PMDDA') or die('Restricted access');

class LoginController extends Controller {

    public function Index() {
        $session = Application::getInstance('Session');
        if (isset($session->user)) {
            $this->request->redirect(Theme::URL('Index/Index'));
        }
        if ($this->request->isPost() && $this->validation()) {
            $this->request->getParam('email');
            $model = new User();
            $data = array(
                'email' => $this->request->getPost('email'),
                'password' => $this->request->getPost('password')
            );
            $user = $model->getUser($data);
            if ($user && !empty($user['status'])) {
                $session->sessionLoggedIn = TRUE;
                $session->user = $user;
                if($user['role'] == 3){
                    $this->request->redirect(Theme::URL('Attendance/Punch'));
                }
                $this->request->redirect(Theme::URL('Index/Index'));
            }
        }
        $data = array();
        $this->display('index', $data);
    }

    protected function validation() {

        if (!filter_var($this->request->getParam('email'), FILTER_VALIDATE_EMAIL) || empty($this->request->getParam('password'))) {
            $this->message->error = I18n::t('Invalid Emai ID or Password');
            return FALSE;
        }
        return true;
    }

    public function Logout() {
        Application::getInstance('Session')->destroy();
        $this->request->redirect(Theme::URL('Login/Index'));
    }

}
