<?php

/**
 * @package ANS Commerce
 * @version 1.0.0
 * @link http://anscommerce.com/
 * @author Nanhe Kumar <nanhe.kumar@gmail.com>
 */
defined('PMDDA') or die('Restricted access');

class IndexController extends Controller {

    public function Index() {
        $data = array();
        $this->display('index', $data);
    }

    

}
