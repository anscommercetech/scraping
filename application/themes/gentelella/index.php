<?php defined('PMDDA') or die('Restricted access'); ?>
<?php $sessionLoggedIn = Application::getInstance('Session')->sessionLoggedIn(); ?>
<?php include('header.php'); ?>

<!-- page content -->
<div class="right_col" role="main">
    
    <?php
    $sucess = isset(View::getMessage()->sucess);
    $error = isset(View::getMessage()->error);
    ?>
    <?php if ($sucess || $error) { ?>
        <div class="alert <?php echo $sucess == true ? 'alert alert-success alert-dismissible fade in' : 'alert-danger alert-dismissible fade in' ?>" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Note:</strong> <?php echo $error == true ? View::getMessage()->error : View::getMessage()->sucess; ?>
        </div>
    <?php } ?>
    
    <?php echo View::getContent(); ?>
</div>
<!-- /page content -->
<?php include('footer.php'); ?>