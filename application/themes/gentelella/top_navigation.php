<style type="text/css">
    .client-name {
    font-size: 16px;
    margin-top: 7px;
    width: 100%;
}
</style>
<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="row client-search" id="global-select-client-warehouse">
                <div class="col-sm-3" style="display:none;">
                    <div class="form-group">
                        <select class="form-control" id="global-select-client" name="client_id">
                            <option value="0"> -Select Client-</option>
                            <option value="1" selected="" >Agent</option>
                            <option value="1" selected="" >Hr Manager</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group client-name"> 
                      Price Comparison Tool 
                    </div>
                </div>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="<?php echo Theme::getPath(); ?>production/images/img_1.jpg" alt=""><?php echo Application::getInstance('Session')->user['name']; ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li style="display: none;"><a href="javascript:;"> Profile</a></li>
                        <li style="display: none;">
                            <a href="javascript:;">
                                <span class="badge bg-red pull-right">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li style="display: none;"><a href="javascript:;">Help</a></li>
                        <li><a href="<?php echo Theme::URL('Login/Logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">

                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->