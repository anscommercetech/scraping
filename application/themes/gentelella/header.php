<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ANS Commerce </title>

        <!-- Bootstrap -->
        <link href="<?php echo Theme::getPath(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo Theme::getPath(); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo Theme::getPath(); ?>vendors/nprogress/nprogress.css" rel="stylesheet">
        <?php
        /**
          <!-- iCheck -->
          <link href="<?php echo Theme::getPath(); ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
         */
        ?>
        <!-- bootstrap-progressbar -->
        <link href="<?php echo Theme::getPath(); ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <?php
        /** */
        ?>
        <!-- Switchery -->
        <link href="<?php echo Theme::getPath(); ?>vendors/switchery/dist/switchery.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="<?php echo Theme::getPath(); ?>vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="<?php echo Theme::getPath(); ?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">     
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
        <!-- Dropzone.js -->
        <link href="<?php echo Theme::getPath(); ?>vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?php echo Theme::getPath(); ?>build/css/custom.css" rel="stylesheet">
        <!-- FullCalendar -->
        <link href="<?php echo Theme::getPath(); ?>vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?php echo Theme::getPath(); ?>vendors/fullcalendar/dist/fullcalendar.print.css" rel="stylesheet" media="print">       
        <!-- jQuery -->
        <script src="<?php echo Theme::getPath(); ?>vendors/jquery/dist/jquery.min.js"></script>
        <!-- Autocomplete -->
          <script src="<?php echo Theme::getPath(); ?>build/js/autocomplete.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo Theme::getPath(); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?php echo Theme::URL('Index/Index') ?>" class="site_title">
                                <i class="fa logo-icon">
                                    <img src="<?php echo Theme::getPath(); ?>production/images/ans-icon.png" alt="..." class="img-circle profile_img">
                                </i> 
                                <span>
                                    <img src="<?php echo Theme::getPath(); ?>production/images/ans-logo.png" alt="..." class="ans logo">
                                </span>
                            </a>
                        </div>

                        <div class="clearfix"></div>
                        <?php if ($sessionLoggedIn) { ?>
                            <!-- menu profile quick info -->
                            <div class="profile clearfix">
                                <div class="profile_pic">
                                    <img src="<?php echo Theme::getPath(); ?>production/images/img_1.jpg" alt="..." class="img-circle profile_img">
                                </div>
                                <div class="profile_info">
                                    <span>Welcome,</span>
                                    <h2><?php echo Application::getInstance('Session')->user['name']; ?></h2>
                                </div>
                            </div>
                            <!-- /menu profile quick info -->

                            <br />
                        <?php } ?>
                        <?php $sessionLoggedIn ? include('sidebar.php') : ''; ?>

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings" style="display: none;">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen" style="display: none;">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock" style="display: none;">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo Theme::URL('Login/Logout') ?>">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <?php $sessionLoggedIn ? include('top_navigation.php') : ''; ?>