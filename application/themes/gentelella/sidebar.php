<?php
$session = Application::getInstance('Session');
$menuList = array(
    array('Index/Index'),
    array('User/Index')
);
?>
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3></h3>
        <ul class="nav side-menu">
            <li><a href="<?php echo Theme::URL('Index/Index') ?>"><i class="fa fa-home"></i> Dashboard </a>
            </li>
            <li><a><i class="fa fa-shopping-basket"></i>Stores<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">    
                    <li><a href="<?php echo Theme::URL('Scraping/urlIndex'); ?>">Websites</a></li>    
                </ul>
                <ul class="nav child_menu">    
                    <li><a href="<?php echo Theme::URL('Scraping/priceIndex'); ?>">Prices</a></li>    
                </ul>

            </li>
            
            <li><a><i class="fa fa-user"></i>Administration<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">    
                    <li><a href="<?php echo Theme::URL('User/index'); ?>">Users</a></li>    
                </ul>
            </li>
        </ul>
        
    </div>
</div>
<!-- /sidebar menu -->