
<!-- footer content -->
<footer>
    <div class="pull-right indexHide">
        <!-- <h1><i class="fa fa-paw"></i> ANS Commerce!</h1> -->
        <p> Powered By <a href= "http://anscommerce.com/"> ANS Commerce </a>.</p>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<script src="<?php echo Theme::getPath(); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<!-- NProgress -->
<script src="<?php echo Theme::getPath(); ?>vendors/nprogress/nprogress.js"></script>

<!-- bootstrap-progressbar -->
<script src="<?php echo Theme::getPath(); ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- DateJS -->
<script src="<?php echo Theme::getPath(); ?>vendors/DateJS/build/date.js"></script>


<!-- JQVMap -->
<script src="<?php echo Theme::getPath(); ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?php echo Theme::getPath(); ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo Theme::getPath(); ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo Theme::getPath(); ?>vendors/moment/min/moment.min.js"></script>
<script src="<?php echo Theme::getPath(); ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- Switchery -->
<script src="<?php echo Theme::getPath(); ?>vendors/switchery/dist/switchery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo Theme::getPath(); ?>vendors/dropzone/dist/min/dropzone.min.js"></script>
<!-- FullCalendar -->
<script src="<?php echo Theme::getPath(); ?>vendors/moment/min/moment.min.js"></script>
<script src="<?php echo Theme::getPath(); ?>vendors/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo Theme::getPath(); ?>build/js/custom.js"></script>
<!-- jquery.inputmask -->
<script src="<?php echo Theme::getPath(); ?>vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<!-- ckeditor -->
<script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>


<script type="text/javascript">
    // for order date picker //
    $('#created-date').datepicker({
        todayBtn: 1,
        autoclose: true
    });
    $('#expiry-date').datepicker({
        todayBtn: 1,
        autoclose: true
    });
    $('#delivery-date').datepicker({
        todayBtn: 1,
        autoclose: true
    });

    $('body').on('click', "#start-date", function () {
        $(this).datepicker();
    });

    $('body').on('click', "#end-date", function () {
        $(this).datepicker();
    });

    $(function () {
        var $dp1 = $("#delivery-date");
        $dp1.datepicker({
            changeYear: true,
            changeMonth: true,
            minDate: 0,

            yearRange: "-100:+20",
        });
    });
    $("#delivery-date").datepicker({
        todayBtn: 1,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#expiry-date').datepicker('setStartDate', minDate);
    });

    $("#expiry-date").datepicker({
        todayBtn: 1,
        autoclose: true,
    })
            .on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#delivery-date').datepicker('setEndDate', minDate);
            });
            
    $(function () {
        var $dp1 = $("#created-date");
        $dp1.datepicker({
            changeYear: true,
            changeMonth: true,
            minDate: 0,

            yearRange: "-100:+20",
        });
    });
    $("#created-date").datepicker({
        todayBtn: 1,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#delivery-date').datepicker('setStartDate', minDate);
    });

    $("#delivery-date").datepicker({
        todayBtn: 1,
        autoclose: true,
    })
            .on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#created-date').datepicker('setEndDate', minDate);
            });
</script>
</body>
</html>
