-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2018 at 11:11 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ans_scraping`
--

-- --------------------------------------------------------

--
-- Table structure for table `scraping_price`
--

CREATE TABLE `scraping_price` (
  `price_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amazon_price` float DEFAULT NULL,
  `flipkart_price` float DEFAULT NULL,
  `myntra_price` float DEFAULT NULL,
  `jabong_price` float DEFAULT NULL,
  `ebay_price` float DEFAULT NULL,
  `snapdeal_price` float DEFAULT NULL,
  `paytmmall_price` float DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scraping_product`
--

CREATE TABLE `scraping_product` (
  `product_id` int(11) NOT NULL,
  `product_sku` varchar(50) NOT NULL,
  `amazon_url` text NOT NULL,
  `flipkart_url` text,
  `myntra_url` text,
  `jabong_url` text,
  `paytmmall_url` text,
  `ebay_url` varchar(40) DEFAULT NULL,
  `snapdeal_url` varchar(40) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scraping_user`
--

CREATE TABLE `scraping_user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `roles` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scraping_user`
--

INSERT INTO `scraping_user` (`user_id`, `name`, `email`, `password`, `status`, `roles`, `created`, `updated`) VALUES
(1, 'Nanhe Kuar', 'nanhe.kumar@gmail.com', '19ea762670230deecc63dac00bfb878a', 1, '', '2018-01-06 00:00:00', '2018-01-10 09:32:07'),
(3, 'Ankit', 'ankit@gmail.com', '447d2c8dc25efbc493788a322f1a00e7', 1, '', '2018-01-06 19:49:17', '2018-01-08 09:57:23'),
(4, 'divyansh', 'divyansh@gmail.com', '10e186315f3d0aad7dce6ee826726509', 3, '', '2018-01-06 19:50:14', '2018-01-06 14:34:47'),
(5, 'randhir', 'randhi@gmail.com', 'f41fe1919b4f026422912381396d55f8', 3, '', '2018-01-06 19:56:22', '2018-01-06 14:36:01'),
(6, 'Divyansh', 'ddsingh11@gmail.com', '263333a8ec19dd5bc4310020bc27f606', 1, '', '2018-01-06 21:05:17', '2018-01-08 09:18:06'),
(7, 'jwef', 'kuhfuhi@gmail.com', 'eee7ec2f8a23cb25f2740143c86a04f2', 1, '', '2018-01-08 01:30:25', '2018-01-08 09:15:23'),
(8, 'Ankit', 'aa@dd.com', 'cd87cd5ef753a06ee79fc75dc7cfe66c', 1, '', '2018-01-08 03:25:58', '2018-01-07 21:55:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scraping_price`
--
ALTER TABLE `scraping_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `scraping_product`
--
ALTER TABLE `scraping_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `scraping_user`
--
ALTER TABLE `scraping_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `scraping_price`
--
ALTER TABLE `scraping_price`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scraping_product`
--
ALTER TABLE `scraping_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scraping_user`
--
ALTER TABLE `scraping_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
